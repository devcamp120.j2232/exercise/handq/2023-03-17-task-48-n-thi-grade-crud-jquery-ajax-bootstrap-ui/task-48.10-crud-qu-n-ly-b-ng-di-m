"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gBASE_URL = "https://62454a477701ec8f724fb923.mockapi.io/api/v1/";

var gStudentList = [];
var gSubjectList = [];
var gGradeList = [];
var gGradeId = "";

const gTABLE_ROW = ["id", "studentId", "subjectId", "grade", "examDate", "action"];
const gSTT_COL = 0;
const gSTUDENT_COL = 1;
const gSUBJECT_COL = 2;
const gGRADE_COL = 3;
const gDATE_COL = 4;
const gACTION_COL = 5;

var gStt = 1;
var gGradeTable = $("#table-grade").DataTable({
    columns: [
        {data: gTABLE_ROW[gSTT_COL]},
        {data: gTABLE_ROW[gSTUDENT_COL]},
        {data: gTABLE_ROW[gSUBJECT_COL]},
        {data: gTABLE_ROW[gGRADE_COL]},
        {data: gTABLE_ROW[gDATE_COL]},
        {data: gTABLE_ROW[gACTION_COL]},
    ],
    columnDefs: [
        {
            targets: gSTT_COL,
            render: function() {
                return gStt++;
            }
        },
        {
            targets: gSTUDENT_COL,
            render: getStudentNameById
        },
        {
            targets: gSUBJECT_COL,
            render: getSubjectNameById
        },
        {
            targets: gACTION_COL,
            defaultContent: 
                `
                    <i class="fas fa-edit fa-lg text-success edit-grade" title="Edit Grade" role="button"></i>
                    <i class="fas fa-trash-alt fa-lg text-danger ml-2 delete-grade"title="Delete Grade" role="button"></i>
                `
        }
    ],
})

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
// gán sự kiện load trang/F5
$(document).ready(function() {
    onPageLoading();
    //Initialize Select2 Elements
    $(".select2").select2();
    // config for select2 to work inside Modal New Grade
    $('#new-select-student, #new-select-subject').select2({
        dropdownParent: $('#modal-new-grade')
    });
    // config for select2 to work inside Modal Edit Grade
    $('#edit-name, #edit-subject').select2({
        dropdownParent: $('#modal-edit-grade')
    });

    //Initialize Datepicker Elements
    $('#new-input-date, #edit-date').datetimepicker({
        timepicker: false,
        format: "d/m/Y"
    });
});

// gán sự kiện click button Lọc dữ liệu
$(document).on("click", "#btn-filter", onBtnFilterClick);

// gán sự kiện click button Thêm Dữ Liệu
$(document).on("click", "#btn-create", onBtnNewGradeClick);
// gán sự kiện click button Thêm điểm thi trên Modal New Grade
$(document).on("click", "#new-btn-create", onBtnNewGradeOnModalClick);

// gán sự kiện click button Edit Grade
$(document).on("click", ".edit-grade", onBtnEditGradeClick);
// gán sự kiện click button Sửa thông tin trên Modal Edit Grade
$(document).on("click", "#edit-btn", onBtnEditGradeOnModalClick);

// gán sự kiện click button Delete Grade
$(document).on("click", ".delete-grade", onBtnDeleteGradeClick);
// gán sự kiện click button Xóa điểm thi trên Modal Delete Grade
$(document).on("click", "#delete-btn", onBtnDeleteGradeOnModalClick);

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// function xử lý sự kiện load trang/F5
function onPageLoading() {
    apiGetStudentList();
    apiGetSubjectList();
    apiGetGradeList();
    loadDataToTable(gGradeList);
    loadStudentListToSelect();
    loadSubjectListToSelect();
}

// function xử lý sự kiện click button Lọc dữ liệu
function onBtnFilterClick() {
    // Khai báo đối tượng chứa dữ liệu
    let vFilterData = {
        studentId: "",
        subjectId: "",
    };
    // B1: Thu thập dữ liệu
    getFilterData(vFilterData);
    // B2: Kiểm tra dữ liệu (không sử dụng)
    // B3: Xử lý hiển thị
    displayFilterData(vFilterData);
}

// function xử lý sự kiện click button Thêm Dữ Liệu
function onBtnNewGradeClick() {
    // hiển thị Modal New Grade
    $("#modal-new-grade").modal("show");
}

// function xử lý sự kiện click button Thêm điểm thi trên Modal New Grade
function onBtnNewGradeOnModalClick() {
    // B1: Khai báo đối tượng và Thu thập dữ liệu
    let vGradeData = {
        studentId: "",
        subjectId: "",
        grade: "",
        examDate: ""
    };
    getNewGradeData(vGradeData);
    // B2: Kiểm tra dữ liệu
    let vDataValid = checkGradeData(vGradeData);
    if (vDataValid) {
        $("#modal-new-grade").modal("hide");
        // B3: Gọi API thêm điểm thi mới
        let vAjaxRequest = apiCreateNewGrade(vGradeData);
        // B4: Xử lý hiển thị
        vAjaxRequest.done(function(response) {
            console.log(response);
            alert("Thêm diểm thi thành công");
            // xóa dữ liệu trên form
            clearInputForm();
            // tải lại dữ liệu điểm thi và load lại table data
            apiGetGradeList();
            loadDataToTable(gGradeList);
        }).fail(function() {
            alert("Thêm điểm thi thất bại");
        });
    }
}

// function xử lý sự kiện click button Edit Grade
function onBtnEditGradeClick() {
    // hiển thị Modal Edit Grade
    $("#modal-edit-grade").modal("show");
    // B1: Thu thập dữ liệu
    getGradeId(this)
    // B2: Kiểm tra dữ liệu (không sử dụng)
    // B3: Gọi API lấy chi tiết grade theo ID
    let vAjaxRequest = apiGetGradeDetailById(gGradeId);
    // B4: Xử lý hiển thị
    vAjaxRequest.done(function(gradeDetail) {
        displayGradeDetail(gradeDetail);
    }).fail(function() {
        alert("Lấy dữ liệu thất bại");
    })
}

// function xử lý sự kiện click button Sửa thông tin trên Modal Edit Grade
function onBtnEditGradeOnModalClick() {
    // Tạo đối tượng chứa dữ liệu
    let vGradeData = {
        studentId: "",
        subjectId: "",
        grade: "",
        examDate: ""
    };
    // B1: Thu thập dữ liệu
    getUpdateGradeData(vGradeData);
    // B2: Kiểm tra dữ liệu
    let vDataValid = checkGradeData(vGradeData);
    if (vDataValid) {
        // ẩn modal
        $("#modal-edit-grade").modal("hide");
        // B3: Gọi API update grade theo Grade ID
        let vAjaxRequest = apiUpdateGradeByGradeId(gGradeId, vGradeData);
        vAjaxRequest.done(function() {
            alert("Update điểm thi thành công");
            // tải lại dữ liệu điểm thi và load lại table data
            apiGetGradeList();
            loadDataToTable(gGradeList);
        }).fail(function() {
            alert("Update điểm thi thất bại");
        });
    }
}

// function xử lý sự kiện click button Delete Grade
function onBtnDeleteGradeClick() {
    $("#modal-delete-grade").modal("show");
    // Thu thập dữ liệu Grade ID lưu vào biến toàn cục gGradeId
    getGradeId(this);
}

// function sự kiện click button Xóa điểm thi trên Modal Delete Grade
function onBtnDeleteGradeOnModalClick() {
    // ẩn modal
    $("#modal-delete-grade").modal("hide");
    // B1: Thu thập dữ liệu
    // B2: Kiểm tra dữ liệu
    // B3: Gọi API xóa grade theo Grade ID
    let vAjaxRequest = apiDeleteGradeByGradeId(gGradeId);
    vAjaxRequest.done(function() {
        alert("Xóa điểm thi thành công");
        // tải lại dữ liệu điểm thi và load lại table data
        apiGetGradeList();
        loadDataToTable(gGradeList);
    }).fail(function() {
        alert("Xóa điểm thi thất bại");
    });
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

//------//
// API  //
//------//

// function gọi API lấy danh sách sinh viên và lưu dữ liệu vào biến toàn cục gStudentList
function apiGetStudentList() {
    return $.ajax({
        url: gBASE_URL + "/students",
        type: "GET",
        async: false,
    }).done(function(studentList) {
        gStudentList = studentList;
    });
}

// function gọi API lấy danh sách môn học và lưu dữ liệu vào biến toàn cục gSubjectList
function apiGetSubjectList() {
    return $.ajax({
        url: gBASE_URL + "/subjects",
        type: "GET",
        async: false,
    }).done(function(subjectList) {
        gSubjectList = subjectList;
    });
}

// function gọi API lấy danh sách điểm và lưu dữ liệu vào biến toàn cục gGradeList
function apiGetGradeList() {
    return $.ajax({
        url: gBASE_URL + "/grades",
        type: "GET",
        async: false,
    }).done(function(gradeList) {
        gGradeList = gradeList;
    });
}

// function gọi API thêm diểm thi mới
function apiCreateNewGrade(paramGradeData) {
    return $.ajax({
        url: gBASE_URL + "/grades",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(paramGradeData),
    });
}

// function gọi API lấy chi tiết grade theo Grade ID
function apiGetGradeDetailById(paramGradeId) {
    return $.ajax({
        url: gBASE_URL + "/grades/" + paramGradeId,
        type: "GET",
    });
}

// function gọi API update grade theo Grade ID
function apiUpdateGradeByGradeId(paramGradeId, paramGradeData) {
    return $.ajax({
        url: gBASE_URL + "/grades/" + paramGradeId,
        type: "PUT",
        contentType: "application/json",
        data: JSON.stringify(paramGradeData),
    })
}

// function gọi API xóa grade theo Grade ID
function apiDeleteGradeByGradeId(paramGradeId) {
    return $.ajax({
        url: gBASE_URL + "/grades/" + paramGradeId,
        type: "DELETE",
    });
}

//---------------------//
// Load data to select //
//---------------------//

// function load Student List vào select
function loadStudentListToSelect() {
    for (let bStudent of gStudentList) {
        $("#select-student, #new-select-student, #edit-name").append(
            $("<option>").val(bStudent.id).text(bStudent.firstname + " " + bStudent.lastname)
        );
    }
}

// function load Subject List vào select
function loadSubjectListToSelect() {
    for (let bSubject of gSubjectList) {
        $("#select-subject, #new-select-subject, #edit-subject").append(
            $("<option>").val(bSubject.id).text(bSubject.subjectName)
        );
    }
}

//------------//
// Data Table //
//------------//

// function load data vào table
function loadDataToTable(paramGradeData) {
    gStt = 1;
    gGradeTable.clear();
    gGradeTable.rows.add(paramGradeData);
    gGradeTable.draw();
}

//----------------//
// Get name by ID //
//----------------//

// function lấy tên sinh viên theo ID
function getStudentNameById(paramStudentId) {
    let vIndex = 0;
    let vFound = false;
    let vStudentName = "";
    while (!vFound && vIndex < gStudentList.length) {
        if (gStudentList[vIndex].id == paramStudentId) {
            vStudentName = gStudentList[vIndex].firstname + " " + gStudentList[vIndex].lastname;
            vFound = true;
        }
        vIndex++;
    }
    return vStudentName;
}

// function lấy tên môn học theo ID
function getSubjectNameById(paramSubjectId) {
    let vIndex = 0;
    let vFound = false;
    let vSubjectName = "";
    while (!vFound && vIndex < gSubjectList.length) {
        if (gSubjectList[vIndex].id == paramSubjectId) {
            vSubjectName = gSubjectList[vIndex].subjectName;
            vFound = true;
        }
        vIndex++;
    }
    return vSubjectName;
}

//--------------//
// Collect Data //
//--------------//

// function thu thập dữ liệu để lọc trên form
// function trả về đối tượng paramFilterData được tham số hóa
function getFilterData(paramFilterData) {
    paramFilterData.studentId = $("#select-student").val();
    paramFilterData.subjectId = $("#select-subject").val();
}

// function thu thập dữ liệu trên form Modal New Grade
// function trả về đối tượng paramGradeData được tham số hóa
function getNewGradeData(paramGradeData) {
    paramGradeData.studentId = $("#new-select-student").val();
    paramGradeData.subjectId = $("#new-select-subject").val();
    paramGradeData.grade = $("#new-input-grade").val().trim();
    paramGradeData.examDate = $("#new-input-date").val().trim();
}

// function thu thập dữ liệu trên form Modal Edit Grade
// function trả về đối tượng paramUpdateGradeData được tham số hóa
function getUpdateGradeData(paramUpdateGradeData) {
    paramUpdateGradeData.studentId = $("#edit-name").val();
    paramUpdateGradeData.subjectId = $("#edit-subject").val();
    paramUpdateGradeData.grade = $("#edit-grade").val().trim();
    paramUpdateGradeData.examDate = $("#edit-date").val().trim();
}

// function lấy dữ liệu Grade ID khi click button Sửa/Xóa và lưu vào biến toàn cục gGradeId
function getGradeId(paramButton) {
    let vRowClicked = $(paramButton).closest("tr");
    let vGradeData = gGradeTable.row(vRowClicked).data();
    gGradeId = vGradeData.id;
}

//------------//
// Check Data //
//------------//

// function kiểm tra dữ liệu trên form Modal New Grade
// function return true nếu tất cả dữ liệu hợp lệ; return false nếu có dữ liệu không hợp lệ
function checkGradeData(paramGradeData) {
    if (paramGradeData.studentId == "") {
        alert("Vui lòng chọn sinh viên");
        return false;
    }
    else if (paramGradeData.subjectId == "") {
        alert("Vui lòng chọn môn học");
        return false;
    }
    else if (paramGradeData.grade == "") {
        alert("Vui lòng nhập điểm thi");
        return false;
    }
    else if (paramGradeData.grade < 0 || paramGradeData.grade > 10) {
        alert("Điểm thi phải lớn hơn hoặc bằng 0 và bé hơn hoặc bằng 10");
        return false;
    }
    else if (paramGradeData.examDate == "") {
        alert("Vui lòng nhập ngày thi");
        return false;
    }
    return true;
}

//--------------//
// Display Data //
//--------------//

// funciton hiển thị dữ liệu lọc theo tên sinh viên và tên môn học vào data table
// nếu user không chọn dữ liệu để lọc thì hiển thị toàn bộ data
function displayFilterData(paramFilterData) {
    let vResult = gGradeList.filter(function(data) {
        return (data.studentId == paramFilterData.studentId || paramFilterData.studentId == "") 
            && (data.subjectId == paramFilterData.subjectId || paramFilterData.subjectId == "")
    })
    loadDataToTable(vResult);
}

// function hiển thị Grade Detail trên Modal Edit Grade
function displayGradeDetail(paramGradeDetail) {
    $("#edit-name").val(paramGradeDetail.studentId).trigger('change');          // .trigger('change') hiển thị thông tin lên select2
    $("#edit-subject").val(paramGradeDetail.subjectId).trigger('change');       // .trigger('change') hiển thị thông tin lên select2
    $("#edit-grade").val(paramGradeDetail.grade);
    $("#edit-date").val(paramGradeDetail.examDate);
}

// function xóa dữ liệu trên form Modal New Grade
function clearInputForm() {
    $("#new-select-student").val("");
    $("#new-select-subject").val("");
    $("#new-input-grade").val("");
    $("#new-input-date").val("");
}